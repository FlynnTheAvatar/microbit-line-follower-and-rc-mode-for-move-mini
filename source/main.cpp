#include "MicroBit.h"

//#define DEBUG 1
//#define PID 1

MicroBit uBit;

const int leftRange = 1200;
const int rightRange = 1200;
const int cipperRanger = 1500;
//speed is from 1 to 90, 1 is slowest, 90 is fastest
#ifdef PID
const int speed = 90;
#else
const int speed = 30;
#endif
// speed + correction must be inside 1 to speed
volatile int servoCorrection = -8;
const float q = 180.0 / 510.0;
#ifdef PID
volatile bool isLineFollowerActive = false;
#endif

#ifndef PID
void handleRise(MicroBitEvent e)
{
  // right
  if (e.source == MICROBIT_ID_IO_P15) {
    // enable servo left
    uBit.io.P1.setServoValue(90 - speed, rightRange);
  }

  // left
  if (e.source == MICROBIT_ID_IO_P16) {
    // enable servo right
    uBit.io.P2.setServoValue(90 + speed + servoCorrection, leftRange);
  }
}

void handleFall(MicroBitEvent e)
{
  // right
  if (e.source == MICROBIT_ID_IO_P15) {
    // disable servo left
    //uBit.io.P1.setDigitalValue(0);
    // stop servo left
    uBit.io.P1.setServoValue(90, rightRange);
  }

  // left
  if (e.source == MICROBIT_ID_IO_P16) {
    // disable servo right
    //uBit.io.P2.setDigitalValue(0);
    // stop servo left
    uBit.io.P2.setServoValue(90, leftRange);
  }
}
#endif

void handleClick(MicroBitEvent e)
{
  if (e.source == MICROBIT_ID_BUTTON_A) {
    servoCorrection--;
    if (speed + servoCorrection < 1) {
      servoCorrection = -speed + 1;
    }
#ifdef DEBUG
    uBit.serial.printf("%d\n", servoCorrection);
#endif
  }
  if (e.source == MICROBIT_ID_BUTTON_B) {
    servoCorrection++;
    if (speed + servoCorrection > 90) {
      servoCorrection = 90 - speed;
    }
#ifdef DEBUG
    uBit.serial.printf("%d\n", servoCorrection);
#endif
  }
  if (e.source == MICROBIT_ID_BUTTON_AB) {
    servoCorrection = 0;
#ifdef DEBUG
    uBit.serial.printf("%d\n", servoCorrection);
#endif
  }
}

void onData(MicroBitEvent e)
{
  uint8_t data[3];
  uBit.radio.datagram.recv(data, 3);
  char s = data[0];
  uint8_t pitch = data[1];
  uint8_t roll = data[2];
#ifdef DEBUG
  uBit.serial.printf("Data received: %c, %d, %d\r\n", s, pitch, roll);
#endif

  // enable line following
  if (s == 'a') {
#ifndef PID
    uBit.io.P15.eventOn(MICROBIT_PIN_EVENT_ON_EDGE);
    uBit.io.P16.eventOn(MICROBIT_PIN_EVENT_ON_EDGE);

    // manually fire event to kick of line following
    if (uBit.io.P15.getDigitalValue() == 1) {
      new MicroBitEvent(MICROBIT_ID_IO_P15, 1, CREATE_AND_FIRE);
    }
    if (uBit.io.P16.getDigitalValue() == 1) {
      new MicroBitEvent(MICROBIT_ID_IO_P16, 1, CREATE_AND_FIRE);
    }
#else
    isLineFollowerActive = true;
#endif
  } else {
    // disable line following
#ifdef PID
    isLineFollowerActive = false;
#else
    uBit.io.P15.eventOn(MICROBIT_PIN_EVENT_NONE);
    uBit.io.P16.eventOn(MICROBIT_PIN_EVENT_NONE);
#endif

    if (s == 'x') {
      // so, we have a value from 0 to 255 for both roll and pitch
      uBit.io.P1.setServoValue((int) ((255 - pitch + roll) * q), rightRange);
      uBit.io.P2.setServoValue((int) ((pitch + roll) * q), leftRange);
    }
    // forward
    if (s == 'f') {
      uBit.io.P1.setServoValue(  0, rightRange);
      uBit.io.P2.setServoValue(180, leftRange);
    }

    // back
    if (s == 'b') {
      uBit.io.P1.setServoValue(180, rightRange);
      uBit.io.P2.setServoValue(  0, leftRange);
    }

    // right
    if (s == 'r') {
      uBit.io.P1.setDigitalValue(0);
      uBit.io.P2.setServoValue(180, leftRange);
    }

    // left
    if (s == 'l') {
      uBit.io.P1.setServoValue(0, rightRange);
      uBit.io.P2.setDigitalValue(0);
    }

    // cipper
    if (s == 'c') {
      uBit.io.P0.setServoValue(180, cipperRanger);
    }

    if (s == 's') {
      uBit.io.P0.setServoValue(0, cipperRanger);
      uBit.io.P1.setDigitalValue(0);
      uBit.io.P2.setDigitalValue(0);
    }
  }
}

int main()
{
#ifdef PID
  register uint8_t leftSensor, rightSensor;
  register uint16_t position;
  register int16_t proportional = 0, lastProportional = 0, derivative = 0, integral = 0;
  float kP = speed / 350.0;
  float kI = 0.0;
  float kD = speed / 120.0;
  register int16_t adjustment = 0;
  register int16_t left, right;
#endif

  uBit.init();
  uBit.display.disable();
#ifdef DEBUG
  uBit.serial.baud(57600);
#endif

  // put servos in neutral position
  uBit.io.P0.setServoValue(0, cipperRanger);
  uBit.io.P1.setDigitalValue(0);
  uBit.io.P2.setDigitalValue(0);

#ifndef PID
  // init buttons
  uBit.messageBus.listen(MICROBIT_ID_BUTTON_A, MICROBIT_BUTTON_EVT_CLICK, handleClick);
  uBit.messageBus.listen(MICROBIT_ID_BUTTON_B, MICROBIT_BUTTON_EVT_CLICK, handleClick);
  uBit.messageBus.listen(MICROBIT_ID_BUTTON_AB, MICROBIT_BUTTON_EVT_CLICK, handleClick);
#endif

  // enable radio
  uBit.messageBus.listen(MICROBIT_ID_RADIO, MICROBIT_RADIO_EVT_DATAGRAM, onData);
  uBit.radio.setGroup(150);
  uBit.radio.enable();

  // init PIN events
  uBit.io.P15.setPull(PullUp);
  uBit.io.P16.setPull(PullUp);
#ifndef PID
  uBit.messageBus.listen(MICROBIT_ID_IO_P15, MICROBIT_PIN_EVT_RISE, handleRise, MESSAGE_BUS_LISTENER_IMMEDIATE);
  uBit.messageBus.listen(MICROBIT_ID_IO_P15, MICROBIT_PIN_EVT_FALL, handleFall, MESSAGE_BUS_LISTENER_IMMEDIATE);
  uBit.messageBus.listen(MICROBIT_ID_IO_P16, MICROBIT_PIN_EVT_RISE, handleRise, MESSAGE_BUS_LISTENER_IMMEDIATE);
  uBit.messageBus.listen(MICROBIT_ID_IO_P16, MICROBIT_PIN_EVT_FALL, handleFall, MESSAGE_BUS_LISTENER_IMMEDIATE);

  // sleep a bit before first detecting the states
  uBit.sleep(500);
  uBit.io.P15.eventOn(MICROBIT_PIN_EVENT_ON_EDGE);
  uBit.io.P16.eventOn(MICROBIT_PIN_EVENT_ON_EDGE);
#endif

  while (true) {
#ifdef PID
    while (isLineFollowerActive) {
      leftSensor = uBit.io.P16.getDigitalValue();
      rightSensor = uBit.io.P15.getDigitalValue();
      if (leftSensor == 0 && rightSensor == 0) {
        // stop motors
        left = 0;
        right = 0;
      } else {
        position = (leftSensor * 0 + rightSensor * 1000) / (leftSensor + rightSensor);
        proportional = position - 500;
        derivative = proportional - lastProportional;
        integral = integral + proportional;
        lastProportional = proportional;

        adjustment = proportional * kP + integral * kI + derivative * kD;
        left = speed - adjustment;
        if (left > speed) left = speed;
        if (left < -speed) left = -speed;
        right = speed + adjustment + servoCorrection;
        if (right > speed) right = speed;
        if (right < -speed) right = -speed;
      }
#ifdef DEBUG
      uBit.serial.printf("left: %d, right: %d, position: %d\r\n", leftSensor, rightSensor, position);
      uBit.serial.printf("prop: %d, derivative: %d, adjustment: %d, left speed: %d, right speed: %d\r\n", proportional, derivative, adjustment, left, right);
#else
      uBit.io.P2.setServoValue(90 + left, leftRange);
      uBit.io.P1.setServoValue(90 - right, rightRange);
#endif

#ifdef DEBUG
      uBit.sleep(1000);
#else
      uBit.sleep(10);
#endif
    }
#else
    uBit.sleep(1000);
#endif
  }

  release_fiber();
  return 0;
}
